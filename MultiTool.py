import tkinter
import random
import uuid
from scapy3k.all import *
from datetime import datetime
import sys


# ***** log
try: # checks if log file exist and reads it 
    file = open("MultiTool_Log.txt", "r")
    string = file.read()
    file.close()
except :
    string = ""

def log(inp, param = 0):
    global string
    string += inp
    if param:
        string += "\n\n -+-+-+- String Write -+-+-+- \n\n"
        file = open("MultiTool_Log.txt","w+")
        file.write(string)
        file.close()
    return string # writes string to log file
log("\n\n ---------- ON_START_UP --------- \n\n")

# *********
# **************************************      Encryptor  functions and params

def from_file():
    inp = tkinter.Tk()
    inp.title("From-File Function")
    frame = tkinter.Frame(inp)
    frame.pack(side = tkinter.RIGHT)

    path_entry = tkinter.Entry(inp,exportselection=0,width = 50,justify = tkinter.LEFT)
    read = tkinter.Button(frame,text = "Read", width = 6, command = lambda: FF_func(path_entry.get(),inp))
    exit = tkinter.Button(frame,text = "Cancel", width = 6, command = lambda: close_window(inp,0))

    path_entry.pack(side = tkinter.LEFT, fill = tkinter.BOTH)
    read.pack(side = tkinter.TOP)
    exit.pack(side = tkinter.BOTTOM)

    inp.mainloop()
    return # maneges read-from-file func (Encryptor)
def close_window(window, param): 
    window.destroy()
    if param == 1:
        Main() 
    elif param == 2:
        modTool_main_func() # closes given window
def FF_func(path,window):
    close_window(window,0)
    if len(path) < 1:
        pop_err("Input Path Invalid! (Too short)\n(Min Length: 1)",10,30,"Read_from_file Path Too Short")
        return 1
    try:
        file = open(path,"r")
        string = file.read()
        file.close()
    except :
       pop_err("File Doesn't Exist!" ,5,20,"Read_from_file File path invalid")
       return 1
    try:
        code = string[:4]
        string = string[4:]
    except:
        pop_err("File Format Invalid!\n(Correct Format: <ENC/DEC>\\n<TEXT TO ENC/DEC>)",10,40,"Read_from_file Format Invalid")
        return 2
    #print(code,"\n"+string[:10])
    if code.startswith("ENC"):
        enc(string)
    elif code.startswith("DEC"):
        dec(string)
    else:
        pop_err("File Format Invalid!\n(Correct Format: <ENC/DEC>\\n<TEXT TO ENC/DEC>)",10,40,"Read_from_file Format Invalid")
    return # read-from-file func (Encryptor)
def pop_err(txt,hght,wid,err_code):
    log(str("ERROR: "+err_code+"\nNEXT;\n\n"))
    err = tkinter.Tk()
    frame = tkinter.Frame(err)
    frame.pack(side = tkinter.BOTTOM)

    lab = tkinter.Label(err,height = hght,width = wid,text = txt)
    exit = tkinter.Button(frame, text = "Ok", width = wid, command = lambda: close_window(err,0))

    lab.pack(side = tkinter.TOP)
    exit.pack(side = tkinter.RIGHT)
    err.mainloop()
    return # pops error string (Encryptor)
def dec_key(strig): # gets key (Encryptor)
    return strig[0]
def dec_chr(strig, key):
    sub = strig[:strig.find(key,1)]#min-32,max-126,change-95
    #print(sub, key)
    #print(key, strig[:strig.find(key,1)], len(sub))
    temp = ord(sub[0]) - (len(sub)-1)
    #print(temp)
    while temp < 32:
        temp += 95
        #print("\b,,,"+str(temp))
    temp = chr(temp)
    #print("\b---"+str(temp))
    strig = strig[strig.find(key,1):]
    return temp, strig # decrypts a single char using key (Encryptor)
def decrypt(strig):
    if len(strig) < 1:
        pop_err("Input String Too Short!\n(Min Length: 1)",10,20,"ENC inp string too short")
        return 1
    temp = strig
    key = strig[0]
    strig += key
    #print(key, strig, len(strig))
    result = ""
    spec = ""
    leng = strig.count(key) - 1
    for i in range(leng):
        c, strig = dec_chr(strig, key)
        result += c
   
    log(str("ENCRYPT\nDEC\nINP: "+temp + "\nOUT: "+result+"\nNEXT;\n\n"))
    
    return result # decrypts a whole string (Encryptor)
def get_letter(): 
    return chr(random.randrange(32,126))#min-32,max-126,change-95   gets a random letter
def steps(inp_chr, trgt_chr):
    i = 0
    while not(inp_chr == trgt_chr):
        inp_chr = chr(ord(inp_chr) + 1)
        if(ord(inp_chr)> 126):
            inp_chr = chr(ord(inp_chr) - 95)
        i+= 1
    return i # counts steps to key (Encryptor)
def create_string(step, trgt):
    strig = ""
    for i in range(step):
        c = chr(random.randrange(32,126))
        while (c == trgt):
            c = chr(random.randrange(32,126))
        strig += c
    
    return strig # creates rand string buy key&chr (Encryptor)
def update_chr(char,trgt):
    step = steps(char, trgt)
    strig = create_string(step,trgt)
    trgt += strig
    return trgt # encrypts a char (Encryptor)
def encrypt(strig):
    if len(strig) < 1:
        pop_err("Input String Too Short!\n(Min Length: 1)",10,20,"ENC inp string too short")
        return 1
    rand = get_letter()
    end_strig = ""
    for char in strig:
        end_strig += update_chr(char, rand)
    log(str("ENCRYPT\nENC\nINP: "+strig + "\nOUT: "+end_strig+"\nNEXT;\n\n"))
   
    
    return end_strig # encrypts a whole string (Encryptor)
def enc(string):
    ret_str = encrypt(string)
    if not(ret_str==1):
        sub_page = tkinter.Tk()
        title = "Encrypted String"
        sub_page.title(title)
        text = tkinter.Text(sub_page, height = 10,width = 70,exportselection=0)
        text.insert(1.0,ret_str)
        text.pack()
        text.configure(state = tkinter.DISABLED)
        text.configure(inactiveselectbackground = text.cget("selectbackground"))
        sub_page.mainloop() # maneges encrypt func (Encryptor)
def dec(string):
    ret_str = decrypt(string)
    if not(ret_str == 1):
        sub_page = tkinter.Tk()
        title = "Decrypted String"
        sub_page.title(title)
        text = tkinter.Text(sub_page, height = 10,width = 70,exportselection=0)
        text.insert(1.0,ret_str)
        text.pack()
        text.configure(state = tkinter.DISABLED)
        text.configure(inactiveselectbackground = text.cget("selectbackground"))
        sub_page.mainloop() # manages decrypt func (Encryptor)
 # ***************************************************************************************

# **********************************************  translator  functions and params
lettar_dic_to_H = { "t":"א", "c":"ב", "d":"ג", "s":"ד", "v":"ה", "u":"ו", "z":"ז", "j":"ח", "y": "ט","h":"י", "f":"כ", "k":"ל", "n":"מ", "b":"נ", "x":"ס", "g":"ע", "p":"פ", "m":"צ", "e":"ק", "r":"ר", "a":"ש", ",":"ת", "o":"ם", "i":"ן", "l":"ך", ";":"ף", ".":"ץ" }
lettar_dic_to_E = { "ש":"a", "נ":"b", "ב":"c", "ג":"d", "ק":"e", "כ":"f", "ע":"g", "י":"h", "ן":"i", "ח":"j", "ל":"k", "ך":"l", "צ":"m", "מ":"n", "ם":"o", "פ":"p", "/":"q", "ר":"r", "ד":"s", "א":"t", "ו":"u", "ה":"v", "'":"w", "ס":"x", "ט":"y", "ז":"z", }
def E_to_H(string):
    sub_page = tkinter.Tk()
    text = tkinter.Text(sub_page, height = 5,width = 50,exportselection=0)
    title = "Transle For: "+string
    sub_page.title(title)
    ret_string = ""
    for char in string:
        if char in str(lettar_dic_to_H.keys()).split("dict_keys([")[1].split("])")[0].replace("'","").replace(" ", "").replace(",",""):
            ret_string += lettar_dic_to_H[char]
        else:
            ret_string += char
    text.insert(1.0,ret_string)
    text.pack()
    text.configure(state = tkinter.DISABLED)
    text.configure(inactiveselectbackground = text.cget("selectbackground"))
    sub_page.mainloop()
    log(str("TRAN\nTanslate From English To Hebrew.\nNEXT;\n\n"))
    return ret_string # translate from English to hebrew
def H_to_E(string):
    sub_page = tkinter.Tk()
    text = tkinter.Text(sub_page, height = 5,width = 50,exportselection=0)
    title = "Transle For: "+string
    sub_page.title(title)
    ret_string = ""
    for char in string:
        if char in str(lettar_dic_to_E.keys()).split("dict_keys([")[1].split("])")[0].replace("'","").replace(" ", "").replace(",",""):
            ret_string += lettar_dic_to_E[char]
        else:
            ret_string += char
    text.insert(1.0,ret_string)
    text.pack()
    text.configure(state = tkinter.DISABLED)
    text.configure(inactiveselectbackground = text.cget("selectbackground"))
    sub_page.mainloop()
    log(str("TRAN\nTanslate From Hebrew To English.\nNEXT;\n\n"))
    return ret_string # trandlate from hebrew to english
# ******************************************************************************

# ******************************************** calculator functions and params
task = ""
UserIn =""
userInput = ""

def create_widgets(self):
    global UserIn
    global user_input
    self.grid()
    """ Create all the buttons for calculator. """
    # User input stored as an Entry widget.
    UserIn = tkinter.StringVar()
    user_input = tkinter.Entry(self,
    insertwidth = 4, width = 24,
    font = ("daivd", 15, "bold"), textvariable = UserIn, justify = tkinter.RIGHT)
    user_input.grid(columnspan = 4)

    user_input.insert(0, "0")

    # Button for value 7
    button1 = tkinter.Button(self,
    text = "7", font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick(7))
    button1.grid(row = 2, column = 0, sticky = tkinter.W)

    # Button for value 8
    button2 = tkinter.Button(self,
    text = "8",   font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick(8))
    button2.grid(row = 2, column = 1, sticky = tkinter.W)

    # Button for value 9
    button3 = tkinter.Button(self,
    text = "9",   font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick(9))
    button3.grid(row = 2, column = 2, sticky = tkinter.W)

    # Button for value 4
    button4 = tkinter.Button(self,
    text = "4",   font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick(4))
    button4.grid(row = 3, column = 0, sticky = tkinter.W)

    # Button for value 5
    button5 = tkinter.Button(self,
    text = "5",   font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick(5))
    button5.grid(row = 3, column = 1, sticky = tkinter.W)

    # Button for value 6
    button6 = tkinter.Button(self, 
    text = "6",   font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick(6))
    button6.grid(row = 3, column = 2, sticky = tkinter.W)

    # Button for value 1
    button7 = tkinter.Button(self,
    text = "1",    font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick(1))
    button7.grid(row = 4, column = 0, sticky = tkinter.W)

    # Button for value 2
    button8 = tkinter.Button(self, 
    text = "2",   font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick(2))
    button8.grid(row = 4, column = 1, sticky = tkinter.W)

    # Button for value 3
    button9 = tkinter.Button(self, 
    text = "3",   font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick(3))
    button9.grid(row = 4, column = 2, sticky = tkinter.W)

    # Button for value 0
    button9 = tkinter.Button(self,
    text = "0",   font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick(0))
    button9.grid(row = 5, column = 0, sticky = tkinter.W)

    # Operator buttons
    # Addition button
    Addbutton = tkinter.Button(self,
    text = "+",  font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick("+"))
    Addbutton.grid(row = 2, column = 3, sticky = tkinter.W)

    # Subtraction button
    Subbutton = tkinter.Button(self,
    text = "-",  font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick("-"))
    Subbutton.grid(row = 3, column = 3, sticky = tkinter.W)

    # Multiplication button
    Multbutton = tkinter.Button(self,
    text = "*", font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick("*"))
    Multbutton.grid(row = 4, column = 3, sticky = tkinter.W)

    # Division button
    Divbutton = tkinter.Button(self,
    text = "/", font = ("daivd", 15, "bold"),width = 5,
    command = lambda : buttonClick("/"))
    Divbutton.grid(row = 5, column = 3, sticky = tkinter.W)

    # Equal button
    Equalbutton = tkinter.Button(self,  
    text = "=", font = ("daivd", 15, "bold"),width = 11,
    command = CalculateTask)
    Equalbutton.grid(row = 5, column = 1, sticky = tkinter.W, columnspan = 2)

    # Clear Button
    Clearbutton = tkinter.Button(self, 
    text = "AC", width = 11, command = ClearDisplay, font = ("daivd", 15, "bold"),)
    Clearbutton.grid(row = 1, columnspan = 2, sticky = tkinter.W)

    backButton = tkinter.Button(self, 
    text = "Back", width = 11, command = lambda: close(self), font = ("daivd", 15, "bold"),)
    backButton.grid(row = 1, columnspan = 2,column = 2, sticky = tkinter.W) # creates widgets for calculator
def buttonClick(number):
    global task
    task = str(task) + str(number)
    UserIn.set(task) # calculates button click (calculator)
def CalculateTask(clear = 0):
    global user_input
    global task
    data = user_input.get()
    try:
        answer = eval(data)
        #print(data,answer,task)
        displayText(answer)
        if not clear ==1:
            task = answer
        else:
            user_input.delete(0, tkinter.END)
            user_input.insert(0, answer)
    except SyntaxError as e:
        ClearDisplay()
        displayText("Invalid Syntax!")
    except ZeroDivisionError as a:
        ClearDisplay()
        displayText("Division By Zero") # calculate string (calculator)
def displayText(value):
    global user_input
    user_input.delete(0, tkinter.END)
    user_input.insert(0, value) # displays text in userInput (calculator)
def ClearDisplay():
    global task
    global user_input
    task = ""
    user_input.delete(0, tkinter.END)
    user_input.insert(0, "0") # clears display (calculator)
def close(self):
    ClearDisplay()
    self.destroy()
    Main() # closes calculator (calculator)
# *********************************************************************************

def call_func(page,name):

    close_window(page,0)
    if name == "enc":
        log(str("CALL\nEncoder\n\n"))
        encoder()
        return
    elif name == "tran":
        log(str("CALL\nTranslator\n\n"))
        translator()
        return
    elif name == "SM mod":
        log(str("CALL\nScrap Mechanic Mod Tool\n\n"))
        modTool_main_func()
        return
    elif name == "calc":
        log(str("CALL\nCalculator\n\n"))
        calculatorFunc()
        return

    return # calls function by str
def Main(): # ********************  main page
    log("\n ---Main Sart--- \n\n")
    mainP = tkinter.Tk()
    mainP.title("MultiTool")
    
    # *************************************************** arrangement frames

    Exit = tkinter.Button(mainP, text = "Exit",width = 12, height = 1, command = lambda: (log("\n ******* ON_EXIT_PRESSED ******* \n\n\n",1),close_window(mainP,0))).grid(row = 20, column = 0,sticky = tkinter.E) # exit

    write = tkinter.Button(mainP, text = "Write Log", width = 12, height = 1, command = lambda: (log("",1)))
    write.grid(row = 20, column = 1, sticky = tkinter.W)

    Func1 = tkinter.Button(mainP, text = "Encryptor",width = 12, height = 1, command = lambda: call_func(mainP, "enc")).grid(row = 0, column = 0, sticky = tkinter.E) # encrypt
    Func2 = tkinter.Button(mainP, text = "Translator",width = 12, height = 1, command = lambda: call_func(mainP, "tran")).grid(row = 0, column = 1, sticky = tkinter.W) # translate

    Func3 = tkinter.Button(mainP, text = "SM Modding", width = 12, height = 1, command = lambda: call_func(mainP, "SM mod")).grid(row = 1, column = 0, sticky = tkinter.E) # sm mod tool

    Func4 = tkinter.Button(mainP, text = "Calculator", width = 12, height = 1, command = lambda: call_func(mainP, "calc")).grid(row = 1, column = 1, sticky = tkinter.W) # calculator

    
    mainP.mainloop()
    return # log:  log(str("\n\n"+"\n\n"))


def encoder():

    main_page = tkinter.Tk()
    title = "Encryptor / Decryptor"
    main_page.title(title)
    frame = tkinter.Frame(main_page)
    sideFrame = tkinter.Frame(frame)
    sideFrameRight = tkinter.Frame(sideFrame)
    frame.pack()
    sideFrame.pack(side = tkinter.RIGHT)
    sideFrameRight.pack(side = tkinter.RIGHT)

    string_entry = tkinter.Entry(frame,exportselection=0,width = 50,justify = tkinter.LEFT)
    Encr = tkinter.Button(sideFrame,text = "Encrypt",command = lambda: enc(string_entry.get()))
    Decr = tkinter.Button(sideFrame,text = "Decrypt",command = lambda: dec(string_entry.get()))
    FF = tkinter.Button(sideFrameRight,text = "Read From File", width = 12, command = from_file)
    back = tkinter.Button(sideFrameRight,text = "Back", width = 12, command = lambda: close_window(main_page,1))

    string_entry.pack(side = tkinter.LEFT, fill = tkinter.BOTH)
    Encr.pack(side = tkinter.TOP)
    Decr.pack(side = tkinter.BOTTOM)
    FF.pack(side = tkinter.TOP)
    back.pack(side = tkinter.BOTTOM)

    main_page.mainloop()
    return # ********** encryptor (Func1)


def translator():
    page = tkinter.Tk()
    title = "Translator"
    page.title(title)
    frame = tkinter.Frame(page)
    sideFrmae = tkinter.Frame(frame)
    rightFrame = tkinter.Frame(sideFrmae)
    frame.pack()
    sideFrmae.pack(side = tkinter.RIGHT)
    rightFrame.pack(side = tkinter.RIGHT)


    string_entry = tkinter.Entry(frame,exportselection=0,width = 50,justify = tkinter.LEFT)
    E_To_H_button = tkinter.Button(sideFrmae,text = "Translate To Hebrow",height = 2,width = 17,command = lambda: E_to_H(string_entry.get()))
    H_To_E_button = tkinter.Button(sideFrmae,text = "Translate To English",height = 2,width = 17,command = lambda: H_to_E(string_entry.get()))
    back = tkinter.Button(rightFrame, text = "Back",height = 5, command= lambda: close_window(page,1))

    string_entry.pack(side = tkinter.LEFT, fill = tkinter.BOTH)
    E_To_H_button.pack(side = tkinter.TOP)
    H_To_E_button.pack(side = tkinter.BOTTOM)
    back.pack(side = tkinter.TOP)
    page.mainloop()

    return # *********** translator (Func2)


# ************** sm modding tool strings
main_string = """   {
    "uuid": " - ",
      "renderable": {
        "lodList": [
          {
            "subMeshList": [
              {
                "textureList": [
                  "$MOD_DATA/Objects/Textures/ - ",
                  "$MOD_DATA/Objects/Textures/ - ",
                  "$MOD_DATA/Objects/Textures/ - "
                ],
                "material": "DifAsgNor"
              }
            ],
            "mesh": "$MOD_DATA/Objects/Mesh/ - " - 
          }
        ]
      },
       - 
      "qualityLevel":  - ,
      "color": " - ",
       - 
      "density":  - ,
      "physicsMaterial": " - ",
      "rotationSet": " - ",
      "sticky": " - "

    }"""
pos0 = """,
            "pose0": "$MOD_DATA/Objects/Mesh/ - "
"""
box = """"box": {
        "x":  - ,
        "y":  - ,
        "z":  - 
      },"""
cylinder = """"cylinder": {
        "diameter":  - ,
        "depth":  - ,
        "margin":  - ,
        "axis": " - "
      },"""
hull = """"hull": { 
				"x":  - , "y":  - , "z":  - ,
				"pointList": [
					{ "x":  - , "y":  - , "z":  -  },
                    { "x":  - , "y":  - , "z":  -  },
                    { "x":  - , "y":  - , "z":  -  }
				]
			},"""
explosive = """"data": {
          "destructionLevel":  - ,
          "destructionRadius":  - ,
          "impulseRadius":  - ,
          "impulseMagnitude":  - ,
          "effectExplosion": " - ",
          "effectActivate": " - "
        }"""
scripted = """"scripted": {
        "filename": "$MOD_DATA/Scripts/ - ",
        "classname": " - ",
         - 
      },"""
meterials = ("Grass","Plastic","Stone","Wood","Metal","Glass","Grid","Potato","Cardboard")
rotationSets = ("Default","Pipe5Way","Pipe4WayBent","PipeCorner","Pipe3Way","PipeStatic","PropFull","PropMirrorYZX","PropZmmNegZmm","PropYmNegYm","PropFullMinusNegY","PropFullMinusNegYX","Window","BunkBed","Bed","SquareBlockY","MirrorXYZ","MirrorXZ","MirrorXY","PropZnegY","PropYNegYmXmZ","PropYXr","PropYX","PropYNegY","PropYZm","PropZmY", "PropZYmX","PropXYZ","PropZY","PropYZ","PropZ","PropY")
sticky = ("+X","-X","+Y","-Y","+Z","-Z")
descr = """" - ": {
    "title": " - ",
    "description": " - "
  },"""

block_string = """    {
      "uuid": " - ",
      "dif": "$MOD_DATA/Objects/Textures/ - ",
      "asg": "$MOD_DATA/Objects/Textures/ - ",
      "nor": "$MOD_DATA/Objects/Textures/ - ",
      "color": " - ",
      "density":  - ,
      "tiling":  - ,
      "physicsMaterial": " - ",
      "qualityLevel":  - 
    }"""

effect_string = """
	{
        "type": " - ",
        "name": " - ",
        "offset": {
          "x":  - ,
          "y":  - ,
          "z":  - 
        },
        "attached":  - ,
        "delay":  - 
      },"""

def modTool_main_func():
    page = tkinter.Tk()
    page.title("SM Mod Tool")

    block = tkinter.Button(page, text = "Block Tool", width = 12, command = lambda: (close_window(page, 0), block_modTool()))
    block.grid(row = 0, column = 0, sticky = tkinter.E)

    part = tkinter.Button(page, text = "Part Tool", width = 12, command = lambda: (close_window(page, 0), modTool()))
    part.grid(row = 0, column = 1, sticky = tkinter.W)

    desc = tkinter.Button(page, text = "Description Tool", width = 12, command = lambda: (close_window(page, 0), description("",2)))
    desc.grid(row = 1, column = 0, sticky = tkinter.E)

    eff = tkinter.Button(page, text = "Effect Tool",width = 12, command = lambda: (close_window(page,0),effect_func()))
    eff.grid(row = 1, column = 1, sticky = tkinter.W)


    Back = tkinter.Button(page, text = "Back",width = 26,height = 1, command = lambda: (log(str("(SM MOD TOOL)\n***SM Mod Tool CloseDown***\nNEXT;\n\n")),close_window(page,1)))
    Back.grid(row = 10,column = 0,columnspan = 2, sticky = tkinter.W)

    page.mainloop()
    return # ******************** sm main tool (func3)

def block_modTool():
    global block_string
    log(str("(SM MOD TOOL)\n---Block Mod Tool StartUp---\nNEXT;\n\n"))
    page = tkinter.Tk()

    page.title("Block Mod Tool")

    uuid_text = tkinter.Label(page, text = "UUID: ", width = 4, height = 1) # uuid
    uuid_text.grid(row = 1, column = 0, sticky = tkinter.E)

    uuid_entry = tkinter.Entry(page, width = 70)
    uuid_entry.grid(row = 1, column = 1, sticky = tkinter.W)

    rand = tkinter.Button(page,width = 10,text = "Randomize",command = lambda: generate_uuid(uuid_entry))
    rand.grid(row = 1, column = 3, sticky = tkinter.W)

    texture1 = tkinter.Label(page, text = "Dif Texture: ", width = 10, height = 1) # dif
    texture1.grid(row = 2, column = 0, sticky = tkinter.E)

    texture1_entry = tkinter.Entry(page, width = 50)
    texture1_entry.grid(row = 2, column = 1, sticky = tkinter.W)
    
    texture2 = tkinter.Label(page, text = "Asg Texture: ",  height = 1) # asg
    texture2.grid(row = 3, column = 0, sticky = tkinter.E)

    texture2_entry = tkinter.Entry(page, width = 50)
    texture2_entry.grid(row = 3, column = 1, sticky = tkinter.W)

    texture3 = tkinter.Label(page, text = "Nor Texture: ",  height = 1) # nor
    texture3.grid(row = 4, column = 0, sticky = tkinter.E)

    texture3_entry = tkinter.Entry(page, width = 50)
    texture3_entry.grid(row = 4, column = 1, sticky = tkinter.W)

    qualityLevel = tkinter.Label(page, text = "QualityLevel:",  height = 1) # qualitylevel
    qualityLevel.grid(row = 18, column = 0, sticky = tkinter.E)
    
    qualityLevel_inp = tkinter.Entry(page, width = 20)
    qualityLevel_inp.grid(row = 18, column = 1, sticky = tkinter.W)

    qualityLevel_help = tkinter.Label(page, text = "Default is 0.",  height = 1)
    qualityLevel_help.grid(row = 18, column = 3, sticky = tkinter.E) # qualitylevel

    color_show = tkinter.Label(page, text = "Color:",  height = 1) # color
    color_show.grid(row = 19, column = 0, sticky = tkinter.E)

    color_inp = tkinter.Entry(page, width = 20)
    color_inp.grid(row = 19, column = 1, sticky = tkinter.W) # color

    color_pick = tkinter.Button(page, text = "Color Picker", command = color_picker)
    color_pick.grid(row = 19, column = 3, sticky = tkinter.E)

    dense_show = tkinter.Label(page, text = "Density:",  height = 1)
    dense_show.grid(row = 34, column = 0, sticky = tkinter.E)

    dense_in = tkinter.Entry(page, width = 20)
    dense_in.grid(row = 34, column = 1, sticky = tkinter.W)# density     uuid_entry, texture1_entry, texture2_entry, texture3_entry, qualityLevel_inp, color_inp, dense_in, material.get(tkinter.ACTIVE), tile_in

    material = tkinter.Listbox(page, width = 50, height = 3) # material
    material.grid(row = 35, column = 1, sticky = tkinter.W)
    global meterials
    for i in meterials:
        material.insert(tkinter.END,i)
    material_show = tkinter.Label(page, text = "Material:",  height = 1)
    material_show.grid(row = 35, column = 0, sticky = tkinter.N)

    tile_show = tkinter.Label(page, text = "Tiling:",  height = 1)
    tile_show.grid(row = 36, column = 0, sticky = tkinter.E)

    tile_in = tkinter.Entry(page, width = 20)
    tile_in.grid(row = 36, column = 1, sticky = tkinter.W)# density

    get = tkinter.Button(page,text = "Generate",width = 10,height = 2,command = lambda: gen_block(uuid_entry, texture1_entry, texture2_entry, texture3_entry, qualityLevel_inp, color_inp, dense_in, material.get(tkinter.ACTIVE), tile_in))
    get.grid(row = 38, column = 1, sticky = tkinter.E)

    back = tkinter.Button(page, text = "Back",width = 15,height = 2, command =  lambda: (close_window(page,2)))
    back.grid(row = 38, column = 3, sticky = tkinter.W)

    desc = tkinter.Button(page, text = "Description", width = 15, height = 1, command = lambda: description(uuid_entry.get(),1))
    desc.grid(row = 37, column = 3, sticky = tkinter.W)


    page.mainloop()

    return # ******************** sm block tool 

def modTool(): 
    log(str("(SM MOD TOOL)\n---Parts Mod Tool StartUp---\nNEXT;\n\n"))
    page = tkinter.Tk()
    page.title("Parts Mod Tool")
    try:
        page.grid()
    except :
        pass

    

    uuid_text = tkinter.Label(page, text = "UUID: ", width = 4, height = 1) # uuid
    uuid_text.grid(row = 1, column = 0, sticky = tkinter.E)

    uuid_entry = tkinter.Entry(page, width = 70)
    uuid_entry.grid(row = 1, column = 1, sticky = tkinter.W)

    rand = tkinter.Button(page,width = 10,text = "Randomize",command = lambda: generate_uuid(uuid_entry))
    rand.grid(row = 1, column = 3, sticky = tkinter.W)

    texture1 = tkinter.Label(page, text = "Dif Texture: ", width = 10, height = 1) # dif
    texture1.grid(row = 2, column = 0, sticky = tkinter.E)

    texture1_entry = tkinter.Entry(page, width = 50)
    texture1_entry.grid(row = 2, column = 1, sticky = tkinter.W)
    
    texture2 = tkinter.Label(page, text = "Asg Texture: ",  height = 1) # asg
    texture2.grid(row = 3, column = 0, sticky = tkinter.E)

    texture2_entry = tkinter.Entry(page, width = 50)
    texture2_entry.grid(row = 3, column = 1, sticky = tkinter.W)

    texture3 = tkinter.Label(page, text = "Nor Texture: ",  height = 1) # nor
    texture3.grid(row = 4, column = 0, sticky = tkinter.E)

    texture3_entry = tkinter.Entry(page, width = 50)
    texture3_entry.grid(row = 4, column = 1, sticky = tkinter.W)

    mesh = tkinter.Label(page, text ="Mesh: ",  height = 1) # mesh
    mesh.grid(row = 5, column = 0, sticky = tkinter.E)

    mesh_entry = tkinter.Entry(page, width = 50)
    mesh_entry.grid(row = 5, column = 1, sticky  = tkinter.W) # mesh

    

    pose0_label = tkinter.Label(page, text = "Pose0: ",  height = 1)
    pose0_label.grid(row = 7, column = 0, sticky = tkinter.E)

    pose0_entry = tkinter.Entry(page, width = 50)
    pose0_entry.grid(row = 7, column = 1, sticky = tkinter.W)
    pose_toggle(pose0_label,pose0_entry, 7,0,"e",7,1,"w")

    pose0 = tkinter.Checkbutton(page, text = "Pose0", height = 1, command = lambda: pose_toggle(pose0_label,pose0_entry, 7,0,"e",7,1,"w")) # pose 0
    pose0.grid(row =6, column = 1, sticky  = tkinter.W)

    script_name = tkinter.Label(page, text = "Script File: ",  height = 1)
    script_name.grid(row = 9, column = 0, sticky = tkinter.E)

    script_classname = tkinter.Label(page, text = "Classname: ",  height = 1)
    script_classname.grid(row = 10, column = 0, sticky = tkinter.E)

    script_name_inp = tkinter.Entry(page, width = 50)
    script_name_inp.grid(row = 9, column = 1, sticky = tkinter.W)

    scrip_classnem_inp = tkinter.Entry(page, width = 50)
    scrip_classnem_inp.grid(row = 10, column = 1, sticky = tkinter.W)
    
    
    exp_destr_lvl_show = tkinter.Label(page, text = "Destruct Level: ", height = 1) # explosives
    exp_destr_lvl_show.grid(row = 12, column = 0, sticky = tkinter.E)

    exp_destr_lvl_inp = tkinter.Entry(page, width = 20)
    exp_destr_lvl_inp.grid(row = 12, column = 1, sticky = tkinter.W)

    exp_destr_radius_show = tkinter.Label(page, text = "Destruct Radius: ")
    exp_destr_radius_show.grid(row = 13, column =  0, sticky = tkinter.E)

    exp_destr_radius_inp = tkinter.Entry(page, width = 20)
    exp_destr_radius_inp.grid(row = 13, column = 1, sticky = tkinter.W)

    impul_radius_show = tkinter.Label(page, text = "Impulse Radius:")
    impul_radius_show.grid(row = 14, column = 0, sticky = tkinter.E)

    impul_radius_inp = tkinter.Entry(page, width = 20)
    impul_radius_inp.grid(row = 14, column = 1, sticky = tkinter.W)

    impul_magnt_show = tkinter.Label(page, text = "Impulse Magnitude:", width = 15)
    impul_magnt_show.grid(row = 15, column = 0, sticky = tkinter.E)

    impul_magnt_inp = tkinter.Entry(page, width = 20)
    impul_magnt_inp.grid(row = 15, column = 1, sticky = tkinter.W)

    eff_explod_show = tkinter.Label(page, text = "Explosion Effect:")
    eff_explod_show.grid(row = 16, column = 0, sticky = tkinter.E)

    eff_explod_inp = tkinter.Entry(page, width = 50)
    eff_explod_inp.grid(row = 16, column = 1, sticky = tkinter.W)

    eff_start_show = tkinter.Label(page, text = "Explosion Effect:")
    eff_start_show.grid(row = 17, column = 0, sticky = tkinter.E)

    eff_start_inp = tkinter.Entry(page, width = 50)
    eff_start_inp.grid(row = 17, column = 1, sticky = tkinter.W) # explosion

    explode_toggle(exp_destr_lvl_show,exp_destr_lvl_inp,exp_destr_radius_show,exp_destr_radius_inp,impul_radius_show,impul_radius_inp,impul_magnt_show,impul_magnt_inp,eff_explod_show,eff_explod_inp,eff_start_show,eff_start_inp)
    explosive = tkinter.Checkbutton(page, text = "Explosvie Script", height = 1, command = lambda: explode_toggle(exp_destr_lvl_show,exp_destr_lvl_inp,exp_destr_radius_show,exp_destr_radius_inp,impul_radius_show,impul_radius_inp,impul_magnt_show,impul_magnt_inp,eff_explod_show,eff_explod_inp,eff_start_show,eff_start_inp))
    explosive.grid(row = 11, column = 1, sticky = tkinter.W) # explosion toggle
    
    script_toggle(script_name,script_name_inp,9,0,"e",9,1,"w",script_classname,scrip_classnem_inp,10,0,"e",10,1,"w",explosive,11,1)

    scrip = tkinter.Checkbutton(page, text = "Script", height = 1, command = lambda: script_toggle(script_name,script_name_inp,9,0,"e",9,1,"w",script_classname,scrip_classnem_inp,10,0,"e",10,1,"w",explosive,11,1)) #  script
    scrip.grid(row = 8, column = 1, sticky = tkinter.W) # script


    qualityLevel = tkinter.Label(page, text = "QualityLevel:",  height = 1) # qualitylevel
    qualityLevel.grid(row = 18, column = 0, sticky = tkinter.E)
    
    qualityLevel_inp = tkinter.Entry(page, width = 20)
    qualityLevel_inp.grid(row = 18, column = 1, sticky = tkinter.W)

    qualityLevel_help = tkinter.Label(page, text = "Default is 0.",  height = 1)
    qualityLevel_help.grid(row = 18, column = 3, sticky = tkinter.E) # qualitylevel

    color_show = tkinter.Label(page, text = "Color:",  height = 1) # color
    color_show.grid(row = 19, column = 0, sticky = tkinter.E)

    color_inp = tkinter.Entry(page, width = 20)
    color_inp.grid(row = 19, column = 1, sticky = tkinter.W) # color

    color_pick = tkinter.Button(page, text = "Color Picker", command = color_picker)
    color_pick.grid(row = 19, column = 3, sticky = tkinter.E)

    # box ************************************************************************************
    x_show = tkinter.Label(page, text = "X:",  height = 1) # x
    x_show.grid(row = 21, column = 0, sticky = tkinter.E)

    x_in = tkinter.Entry(page, width = 20)
    x_in.grid(row = 21,column = 1, sticky = tkinter.W)

    y_show = tkinter.Label(page, text = "Y:",  height = 1) # y
    y_show.grid(row = 22, column = 0, sticky = tkinter.E)

    y_in = tkinter.Entry(page, width = 20)
    y_in.grid(row = 22,column = 1, sticky = tkinter.W)

    z_show = tkinter.Label(page, text = "Z:",  height = 1) # z
    z_show.grid(row = 24, column = 0, sticky = tkinter.E)

    z_in = tkinter.Entry(page, width = 20)
    z_in.grid(row = 24,column = 1, sticky = tkinter.W) # end ***************************

    # cylinder *******************************************************************************************
    diameter_show = tkinter.Label(page, text = "Diameter:",  height = 1) # Diameter
    diameter_show.grid(row = 21, column = 0, sticky = tkinter.E)

    diameter_in = tkinter.Entry(page, width = 20)
    diameter_in.grid(row = 21,column = 1, sticky = tkinter.W)

    depth_show = tkinter.Label(page, text = "Depth:",  height = 1) # Depth
    depth_show.grid(row = 22, column = 0, sticky = tkinter.E)

    depth_in = tkinter.Entry(page, width = 20)
    depth_in.grid(row = 22,column = 1, sticky = tkinter.W)

    marg_show = tkinter.Label(page, text = "Margin:",height = 1) #marg_show,marg_in
    marg_show.grid(row = 23,column = 0, sticky = tkinter.E)

    marg_in = tkinter.Entry(page,width = 20)
    marg_in.grid(row = 23, column = 1, sticky = tkinter.W)

    axis_show = tkinter.Label(page, text = "Axis:",  height = 1) # Axis
    axis_show.grid(row = 24, column = 0, sticky = tkinter.E)

    axis_in = tkinter.Entry(page, width = 20)
    axis_in.grid(row = 24,column = 1, sticky = tkinter.W) # end *************************************** x_show x_in y_show y_in z_show z_in - diameter_show diameter_in depth_show depth_in axis_show axis_in - h_x_show h_x_in h_y_show h_y_in h_z_show h_z_in Pl1_x_show Pl1_x_in Pl1_y_show Pl1_y_in Pl1_z_show Pl1_z_in Pl2_x_show Pl2_x_in Pl2_y_show Pl2_y_in Pl2_z_show Pl2_z_in Pl3_x_show Pl3_x_in Pl3_y_show Pl3_y_in Pl3_z_show Pl3_z_in 

    # HULL -----------------------------------------------------------------------------------------------------------
    h_x_show = tkinter.Label(page, text = "X:",  height = 1) # x
    h_x_show.grid(row = 21, column = 0, sticky = tkinter.E)

    h_x_in = tkinter.Entry(page, width = 20)
    h_x_in.grid(row = 21,column = 1, sticky = tkinter.W)

    h_y_show = tkinter.Label(page, text = "Y:",  height = 1) # y
    h_y_show.grid(row = 22, column = 0, sticky = tkinter.E)

    h_y_in = tkinter.Entry(page, width = 20)
    h_y_in.grid(row = 22,column = 1, sticky = tkinter.W)

    h_z_show = tkinter.Label(page, text = "Z:",  height = 1) # z
    h_z_show.grid(row = 24, column = 0, sticky = tkinter.E)

    h_z_in = tkinter.Entry(page, width = 20)
    h_z_in.grid(row = 24,column = 1, sticky = tkinter.W) 
    # pointlist 1 
    Pl1_x_show = tkinter.Label(page, text = "PointList X:",  height = 1) # x **** 
    Pl1_x_show.grid(row = 25, column = 0, sticky = tkinter.E)

    Pl1_x_in = tkinter.Entry(page, width = 20)
    Pl1_x_in.grid(row = 25,column = 1, sticky = tkinter.W)

    Pl1_y_show = tkinter.Label(page, text = "PointList Y:",  height = 1) # y
    Pl1_y_show.grid(row = 26, column = 0, sticky = tkinter.E)

    Pl1_y_in = tkinter.Entry(page, width = 20)
    Pl1_y_in.grid(row = 26,column = 1, sticky = tkinter.W)

    Pl1_z_show = tkinter.Label(page, text = "PointList Z:",  height = 1) # z
    Pl1_z_show.grid(row = 27, column = 0, sticky = tkinter.E)

    Pl1_z_in = tkinter.Entry(page, width = 20)
    Pl1_z_in.grid(row = 27,column = 1, sticky = tkinter.W) 

    #sideFrame1 = tkinter.Frame(page)
    #sideFrame1.grid(row = 27, column = 2, sticky = tkinter.E)

    #add_button1 = tkinter.Button(sideFrame1, text = "Add", width =  10, height = 1)
    #add_button1.pack(side = tkinter.LEFT) 
    
    #rem_button1 = tkinter.Button(sideFrame1, text = "Remove", width =  10, height = 1)
    #rem_button1.pack(side = tkinter.RIGHT) # end

    # pointlist 2 
    Pl2_x_show = tkinter.Label(page, text = "PointList X:",  height = 1) # x
    Pl2_x_show.grid(row = 28, column = 0, sticky = tkinter.E)

    Pl2_x_in = tkinter.Entry(page, width = 20)
    Pl2_x_in.grid(row = 28,column = 1, sticky = tkinter.W)

    Pl2_y_show = tkinter.Label(page, text = "PointList Y:",  height = 1) # y
    Pl2_y_show.grid(row = 29, column = 0, sticky = tkinter.E)

    Pl2_y_in = tkinter.Entry(page, width = 20)
    Pl2_y_in.grid(row = 29,column = 1, sticky = tkinter.W)

    Pl2_z_show = tkinter.Label(page, text = "PointList Z:",  height = 1) # z
    Pl2_z_show.grid(row = 30, column = 0, sticky = tkinter.E)

    Pl2_z_in = tkinter.Entry(page, width = 20)
    Pl2_z_in.grid(row = 30,column = 1, sticky = tkinter.W) 

    # pointlist 3
    Pl3_x_show = tkinter.Label(page, text = "PointList X:",  height = 1) # x
    Pl3_x_show.grid(row = 31, column = 0, sticky = tkinter.E)

    Pl3_x_in = tkinter.Entry(page, width = 20)
    Pl3_x_in.grid(row = 31,column = 1, sticky = tkinter.W)

    Pl3_y_show = tkinter.Label(page, text = "PointList Y:",  height = 1) # y
    Pl3_y_show.grid(row = 32, column = 0, sticky = tkinter.E)

    Pl3_y_in = tkinter.Entry(page, width = 20)
    Pl3_y_in.grid(row = 32,column = 1, sticky = tkinter.W)

    Pl3_z_show = tkinter.Label(page, text = "PointList Z:",  height = 1) # z
    Pl3_z_show.grid(row = 33, column = 0, sticky = tkinter.E)

    Pl3_z_in = tkinter.Entry(page, width = 20)
    Pl3_z_in.grid(row = 33,column = 1, sticky = tkinter.W) 

    # Hull End -------------------------------------------------------------------------------------------------------
    di_hul(h_x_show ,h_x_in ,h_y_show ,h_y_in ,h_z_show ,h_z_in ,Pl1_x_show ,Pl1_x_in ,Pl1_y_show ,Pl1_y_in ,Pl1_z_show ,Pl1_z_in ,Pl2_x_show ,Pl2_x_in ,Pl2_y_show ,Pl2_y_in ,Pl2_z_show ,Pl2_z_in ,Pl3_x_show ,Pl3_x_in ,Pl3_y_show, Pl3_y_in, Pl3_z_show ,Pl3_z_in)
    di_cy(diameter_show ,diameter_in ,depth_show, depth_in ,axis_show, axis_in,marg_show,marg_in)
    frame = tkinter.Frame(page)
    frame.grid(row = 20, column = 1, sticky = tkinter.W) # frame
    
    box_but = tkinter.Button(page, text = "Box", width = 6, height = 1, command = lambda: hitbox("BOX",   x_show, x_in, y_show ,y_in ,z_show ,z_in      ,diameter_show ,diameter_in ,depth_show, depth_in ,marg_show,marg_in,axis_show, axis_in,      h_x_show ,h_x_in ,h_y_show ,h_y_in ,h_z_show ,h_z_in ,Pl1_x_show ,Pl1_x_in ,Pl1_y_show ,Pl1_y_in ,Pl1_z_show ,Pl1_z_in ,Pl2_x_show ,Pl2_x_in ,Pl2_y_show ,Pl2_y_in ,Pl2_z_show ,Pl2_z_in ,Pl3_x_show ,Pl3_x_in ,Pl3_y_show, Pl3_y_in, Pl3_z_show ,Pl3_z_in ))
    box_but.grid(row = 20, column = 0, sticky = tkinter.E)

    cyl_but = tkinter.Button(frame, text = "Cylinder", width = 6, height = 1, command = lambda: hitbox("CYL",   x_show, x_in, y_show ,y_in ,z_show ,z_in      ,diameter_show ,diameter_in ,depth_show, depth_in ,marg_show,marg_in,axis_show, axis_in,      h_x_show ,h_x_in ,h_y_show ,h_y_in ,h_z_show ,h_z_in ,Pl1_x_show ,Pl1_x_in ,Pl1_y_show ,Pl1_y_in ,Pl1_z_show ,Pl1_z_in ,Pl2_x_show ,Pl2_x_in ,Pl2_y_show ,Pl2_y_in ,Pl2_z_show ,Pl2_z_in ,Pl3_x_show ,Pl3_x_in ,Pl3_y_show, Pl3_y_in, Pl3_z_show ,Pl3_z_in ))
    hul_but = tkinter.Button(frame, text = "Hull", width = 6, height = 1, command = lambda: hitbox("HUL",   x_show, x_in, y_show ,y_in ,z_show ,z_in      ,diameter_show ,diameter_in ,depth_show, depth_in ,marg_show,marg_in,axis_show, axis_in,      h_x_show ,h_x_in ,h_y_show ,h_y_in ,h_z_show ,h_z_in ,Pl1_x_show ,Pl1_x_in ,Pl1_y_show ,Pl1_y_in ,Pl1_z_show ,Pl1_z_in ,Pl2_x_show ,Pl2_x_in ,Pl2_y_show ,Pl2_y_in ,Pl2_z_show ,Pl2_z_in ,Pl3_x_show ,Pl3_x_in ,Pl3_y_show, Pl3_y_in, Pl3_z_show ,Pl3_z_in ))
    cyl_but.pack(side = tkinter.LEFT)
    hul_but.pack(side = tkinter.RIGHT)
    # hitbox END ****************************************************-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*---****+*+*+*+*+*+*+**+*+*+*++*++*+

    dense_show = tkinter.Label(page, text = "Density:",  height = 1)
    dense_show.grid(row = 34, column = 0, sticky = tkinter.E)

    dense_in = tkinter.Entry(page, width = 20)
    dense_in.grid(row = 34, column = 1, sticky = tkinter.W)# density

    material = tkinter.Listbox(page, width = 50, height = 3) # material
    material.grid(row = 35, column = 1, sticky = tkinter.W)
    global meterials
    for i in meterials:
        material.insert(tkinter.END,i)
    material_show = tkinter.Label(page, text = "Material:",  height = 1)
    material_show.grid(row = 35, column = 0, sticky = tkinter.N)


    rotate = tkinter.Listbox(page, width = 50, height = 3) # rotationsets
    rotate.grid(row = 36, column = 1, sticky = tkinter.W)
    global rotationSets
    for i in rotationSets:
        rotate.insert(tkinter.END,i)
    rotate_show = tkinter.Label(page, text = "RotationSet:",  height = 1)
    rotate_show.grid(row = 36, column = 0, sticky = tkinter.N)

    rotate_help = tkinter.Label(page, text = "Default is PropYZ\n(At the end)",width = 15, height = 2)
    rotate_help.grid(row = 36, column = 3, sticky = tkinter.W) # rotationsets

    stick_show = tkinter.Label(page,  height = 1, text = "Sticky:")
    stick_show.grid(row = 37, column = 0, sticky = tkinter.E)

    # sticky frames

    m_frame = tkinter.Frame(page)
    m_frame.grid(row = 37, column = 1, sticky = tkinter.W)

    big_left_frame = tkinter.Frame(m_frame)
    big_left_frame.pack(side = tkinter.LEFT)

    right_frame = tkinter.Frame(m_frame)
    right_frame.pack(side = tkinter.RIGHT)

    small_left_frame = tkinter.Frame(big_left_frame)
    small_left_frame.pack(side = tkinter.LEFT)

    small_right = tkinter.Frame(big_left_frame)
    small_right.pack(side = tkinter.RIGHT)

    # sticky frames

    x = tkinter.IntVar()
    stick_in_x = tkinter.Checkbutton(small_left_frame, text = "X+", variable=x)
    stick_in_x.pack(side = tkinter.LEFT)

    x1 = tkinter.IntVar()
    stick_in_x_min = tkinter.Checkbutton(small_left_frame, text = "X-", variable=x1)
    stick_in_x_min.pack(side = tkinter.RIGHT)

    y = tkinter.IntVar()
    stick_in_y = tkinter.Checkbutton(small_right, text = "Y+", variable=y)
    stick_in_y.pack(side = tkinter.LEFT)

    y1 = tkinter.IntVar()
    stick_in_y_min = tkinter.Checkbutton(small_right, text = "Y-", variable=y1)
    stick_in_y_min.pack(side = tkinter.RIGHT)

    z = tkinter.IntVar()
    stick_in_z = tkinter.Checkbutton(right_frame, text = "z+", variable=z)
    stick_in_z.pack(side = tkinter.LEFT)

    z1 = tkinter.IntVar()
    stick_in_z_min = tkinter.Checkbutton(right_frame, text = "z-", variable=z1)
    stick_in_z_min.pack(side = tkinter.RIGHT)
    
    
    Generate = tkinter.Button(page,text = "Generate", width = 10, height = 2, command = lambda: GetAll(uuid_entry,texture1_entry,texture2_entry,texture3_entry,mesh_entry,  pose0_entry,   script_name_inp,scrip_classnem_inp,   exp_destr_lvl_inp,exp_destr_radius_inp,impul_radius_inp,impul_magnt_inp,eff_explod_inp,eff_start_inp,  qualityLevel_inp,color_inp,  dense_in,  material,rotate,  x_in,y_in,z_in   ,diameter_in, depth_in,marg_in, axis_in   ,h_x_in,h_y_in,h_z_in,Pl1_x_in,Pl1_y_in,Pl1_z_in,Pl2_x_in ,Pl2_y_in ,Pl2_z_in ,Pl3_x_in ,Pl3_y_in,Pl3_z_in,  x,x1,y,y1,z,z1))
    Generate.grid(row = 38,column = 1,rowspan = 2, sticky = tkinter.E)

    Back = tkinter.Button(page, text = "Back",width = 15,height = 2, command = lambda: (log(str("(SM MOD TOOL)\n***Parts Tool CloseDown***\nNEXT;\n\n")),close_window(page,2)))
    Back.grid(row = 38,column = 3,rowspan = 2, sticky = tkinter.W)
    
    desc = tkinter.Button(page, text = "Description", width = 15, height = 1, command = lambda: description(uuid_entry.get(),1))
    desc.grid(row = 37, column = 3, sticky = tkinter.W)

    #Back.pack(side = tkinter.TOP)

    page.mainloop()
    return # ******************** sm part tool
pose0_op = True
script_op = True
explosive_op = True
shape = "BOX" # BOX / CYL / HUL
def explode_toggle(power,power1,p_range,p_range1,r_impulse,r_impulse1,p_impulse,p_impulse1,exp_eff,exp_eff1,start_eff, start_eff1):
    global explosive_op

    if explosive_op:
        power.grid_remove()
        power1.grid_remove()
        p_range.grid_remove()
        p_range1.grid_remove()
        r_impulse.grid_remove()
        r_impulse1.grid_remove()
        p_impulse.grid_remove()
        p_impulse1.grid_remove()
        exp_eff.grid_remove()
        exp_eff1.grid_remove()
        start_eff.grid_remove()
        start_eff1.grid_remove()
        explosive_op = False
    else:
        power.grid(row = 12,column = 0,sticky = tkinter.E)
        power1.grid(row = 12,column = 1, sticky = tkinter.W)
        p_range.grid(row = 13,column = 0,sticky = tkinter.E)
        p_range1.grid(row = 13,column = 1, sticky = tkinter.W)
        r_impulse.grid(row = 14,column = 0,sticky = tkinter.E)
        r_impulse1.grid(row = 14,column =1, sticky = tkinter.W)
        p_impulse.grid(row = 15,column = 0,sticky = tkinter.E)
        p_impulse1.grid(row = 15,column = 1, sticky = tkinter.W)
        exp_eff.grid(row = 16,column =0,sticky = tkinter.E)
        exp_eff1.grid(row = 16,column = 1, sticky = tkinter.W)
        start_eff.grid(row = 17,column = 0,sticky = tkinter.E)
        start_eff1.grid(row = 17,column = 1, sticky = tkinter.W)
        explosive_op = True
    #print("explode",explosive_op)
    return # toggles explosives script visibility

def script_toggle(pageA, pageB, R, C, stick, R1, C1, stick1,pageA_b, pageB_b, R_b, C_b, stick_b, R1_b, C1_b, stick1_b, expl_check,exp_r,exp_c):
    global script_op
    if script_op:
        pageA.grid_remove()
        pageB.grid_remove()
        pageA_b.grid_remove()
        pageB_b.grid_remove()
        expl_check.grid_remove()
        script_op = False
    else:
        pageA.grid(row = R, column = C, sticky = tkinter.E)
        pageB.grid(row = R1, column = C1, sticky = tkinter.W)
        pageA_b.grid(row = R_b, column = C_b, sticky = tkinter.E)
        pageB_b.grid(row = R1_b,column = C1_b, sticky = tkinter.W)
        expl_check.grid(row =  11, column = 1, sticky = tkinter.W)
        script_op = True
    #print("script",script_op)
    return # toggles script visibility

def pose_toggle(pageA, pageB, R, C, stick, R1, C1, stick1):
    global pose0_op
    if pose0_op:
        pageA.grid_remove()
        pageB.grid_remove()
        pose0_op = False
    else:
        pageA.grid(row = R, column = C, sticky = tkinter.E)
        pageB.grid(row = R1, column = C1, sticky = tkinter.W)
        pose0_op = True
    #print("pose",pose0_op)
    return # toggles pose0 visiblity

def hitbox(type,   x_show, x_in, y_show ,y_in ,z_show ,z_in      ,diameter_show ,diameter_in ,depth_show, depth_in ,marg_show,marg_in,axis_show, axis_in,      h_x_show ,h_x_in ,h_y_show ,h_y_in ,h_z_show ,h_z_in ,Pl1_x_show ,Pl1_x_in ,Pl1_y_show ,Pl1_y_in ,Pl1_z_show ,Pl1_z_in ,Pl2_x_show ,Pl2_x_in ,Pl2_y_show ,Pl2_y_in ,Pl2_z_show ,Pl2_z_in ,Pl3_x_show ,Pl3_x_in ,Pl3_y_show, Pl3_y_in, Pl3_z_show ,Pl3_z_in ):
    global shape
    if type == "BOX":
        en_xyz(x_show, x_in, y_show ,y_in ,z_show ,z_in)
        di_cy(diameter_show ,diameter_in ,depth_show, depth_in ,axis_show, axis_in,marg_show,marg_in)
        di_hul(h_x_show ,h_x_in ,h_y_show ,h_y_in ,h_z_show ,h_z_in ,Pl1_x_show ,Pl1_x_in ,Pl1_y_show ,Pl1_y_in ,Pl1_z_show ,Pl1_z_in ,Pl2_x_show ,Pl2_x_in ,Pl2_y_show ,Pl2_y_in ,Pl2_z_show ,Pl2_z_in ,Pl3_x_show ,Pl3_x_in ,Pl3_y_show ,Pl3_y_in ,Pl3_z_show ,Pl3_z_in)
    elif type == "CYL":
        en_cy(diameter_show ,diameter_in ,depth_show, depth_in ,axis_show, axis_in,marg_show,marg_in)
        di_xyz(x_show, x_in, y_show ,y_in ,z_show ,z_in)
        di_hul(h_x_show ,h_x_in ,h_y_show ,h_y_in ,h_z_show ,h_z_in ,Pl1_x_show ,Pl1_x_in ,Pl1_y_show ,Pl1_y_in ,Pl1_z_show ,Pl1_z_in ,Pl2_x_show ,Pl2_x_in ,Pl2_y_show ,Pl2_y_in ,Pl2_z_show ,Pl2_z_in ,Pl3_x_show ,Pl3_x_in ,Pl3_y_show ,Pl3_y_in ,Pl3_z_show ,Pl3_z_in)
    else:
        en_hul(h_x_show ,h_x_in ,h_y_show ,h_y_in ,h_z_show ,h_z_in ,Pl1_x_show ,Pl1_x_in ,Pl1_y_show ,Pl1_y_in ,Pl1_z_show ,Pl1_z_in ,Pl2_x_show ,Pl2_x_in ,Pl2_y_show ,Pl2_y_in ,Pl2_z_show ,Pl2_z_in ,Pl3_x_show ,Pl3_x_in ,Pl3_y_show ,Pl3_y_in ,Pl3_z_show ,Pl3_z_in)
        di_xyz(x_show, x_in, y_show ,y_in ,z_show ,z_in)
        di_cy(diameter_show ,diameter_in ,depth_show, depth_in ,axis_show, axis_in,marg_show,marg_in)
    shape = type
    return # disables/enambles hitboxes b type value
def en_xyz(x_show, x_in, y_show ,y_in ,z_show ,z_in):
    x_show.grid(row = 21, column = 0, sticky = tkinter.E)
    x_in.grid(row = 21, column = 1, sticky = tkinter.W)
    y_show.grid(row = 22, column = 0, sticky = tkinter.E)
    y_in.grid(row = 22, column = 1, sticky = tkinter.W)
    z_show.grid(row = 24, column = 0, sticky = tkinter.E)
    z_in.grid(row = 24, column = 1, sticky = tkinter.W)
    return # en = enable, di = disable. xyz = BOX, cy = cylinder, hul = Hull
def di_xyz(x_show, x_in, y_show ,y_in ,z_show ,z_in):
    x_show.grid_remove()
    x_in.grid_remove()
    y_show.grid_remove()
    y_in.grid_remove()
    z_show.grid_remove()
    z_in.grid_remove()
    return

def en_cy(diameter_show ,diameter_in ,depth_show, depth_in ,axis_show, axis_in,marg_show,marg_in):
    diameter_show.grid(row = 21, column = 0, sticky = tkinter.E)
    diameter_in.grid(row = 21, column = 1, sticky = tkinter.W)
    depth_show.grid(row = 22, column = 0, sticky = tkinter.E)
    depth_in.grid(row = 22, column = 1, sticky = tkinter.W)
    marg_show.grid()
    marg_in.grid()
    axis_show.grid(row = 24, column = 0, sticky = tkinter.E)
    axis_in.grid(row = 24, column = 1, sticky = tkinter.W)
    return
def di_cy(diameter_show ,diameter_in ,depth_show, depth_in ,axis_show, axis_in,marg_show,marg_in):
    diameter_show.grid_remove()
    diameter_in.grid_remove()
    depth_show.grid_remove()
    depth_in.grid_remove()
    marg_show.grid_remove()
    marg_in.grid_remove()
    axis_show.grid_remove()
    axis_in.grid_remove()
    return

def en_hul(h_x_show ,h_x_in ,h_y_show ,h_y_in ,h_z_show ,h_z_in ,Pl1_x_show ,Pl1_x_in ,Pl1_y_show ,Pl1_y_in ,Pl1_z_show ,Pl1_z_in ,Pl2_x_show ,Pl2_x_in ,Pl2_y_show ,Pl2_y_in ,Pl2_z_show ,Pl2_z_in ,Pl3_x_show ,Pl3_x_in ,Pl3_y_show, Pl3_y_in, Pl3_z_show ,Pl3_z_in):
    h_x_show.grid(row = 21, column = 0, sticky = tkinter.E)
    h_x_in.grid(row = 21, column = 1, sticky = tkinter.W)
    h_y_show.grid(row = 22, column = 0, sticky = tkinter.E)
    h_y_in.grid(row = 22, column = 1, sticky = tkinter.W)
    h_z_show.grid(row = 24, column = 0, sticky = tkinter.E)
    h_z_in.grid(row = 24, column = 1, sticky = tkinter.W)
    Pl1_x_show.grid(row = 25, column = 0, sticky = tkinter.E)
    Pl1_x_in.grid(row = 25, column = 1, sticky = tkinter.W)
    Pl1_y_show.grid(row = 26, column = 0, sticky = tkinter.E)
    Pl1_y_in.grid(row = 26, column = 1, sticky = tkinter.W)
    Pl1_z_show.grid(row = 27, column = 0, sticky = tkinter.E)
    Pl1_z_in.grid(row = 27, column = 1, sticky = tkinter.W)
    Pl2_x_show.grid(row = 28, column = 0, sticky = tkinter.E)
    Pl2_x_in.grid(row = 28, column = 1, sticky = tkinter.W)
    Pl2_y_show.grid(row = 29, column = 0, sticky = tkinter.E)
    Pl2_y_in.grid(row = 29, column = 1, sticky = tkinter.W)
    Pl2_z_show.grid(row = 30, column = 0, sticky = tkinter.E)
    Pl2_z_in.grid(row = 30, column = 1, sticky = tkinter.W)
    Pl3_x_show.grid(row = 31, column = 0, sticky = tkinter.E)
    Pl3_x_in.grid(row = 31, column = 1, sticky = tkinter.W)
    Pl3_y_show.grid(row = 32, column = 0, sticky = tkinter.E)
    Pl3_y_in.grid(row = 32, column = 1, sticky = tkinter.W)
    Pl3_z_show.grid(row = 33, column = 0, sticky = tkinter.E)
    Pl3_z_in.grid(row = 33, column = 1, sticky = tkinter.W)
    return
def di_hul(h_x_show ,h_x_in ,h_y_show ,h_y_in ,h_z_show ,h_z_in ,Pl1_x_show ,Pl1_x_in ,Pl1_y_show ,Pl1_y_in ,Pl1_z_show ,Pl1_z_in ,Pl2_x_show ,Pl2_x_in ,Pl2_y_show ,Pl2_y_in ,Pl2_z_show ,Pl2_z_in ,Pl3_x_show ,Pl3_x_in ,Pl3_y_show ,Pl3_y_in ,Pl3_z_show ,Pl3_z_in):
    h_x_show.grid_remove()
    h_x_in.grid_remove()
    h_y_show.grid_remove()
    h_y_in.grid_remove()
    h_z_show.grid_remove()
    h_z_in.grid_remove()
    Pl1_x_show.grid_remove()
    Pl1_x_in.grid_remove()
    Pl1_y_show.grid_remove()
    Pl1_y_in.grid_remove()
    Pl1_z_show.grid_remove()
    Pl1_z_in.grid_remove()
    Pl2_x_show.grid_remove()
    Pl2_x_in.grid_remove()
    Pl2_y_show.grid_remove()
    Pl2_y_in.grid_remove()
    Pl2_z_show.grid_remove()
    Pl2_z_in.grid_remove()
    Pl3_x_show.grid_remove()
    Pl3_x_in.grid_remove()
    Pl3_y_show.grid_remove()
    Pl3_y_in.grid_remove()
    Pl3_z_show.grid_remove()
    Pl3_z_in.grid_remove()
    return
 
def inject(param_list, string,divide):
    string = string.split(divide)
    #print("param: "+str(len(param_list)),"string: "+str(len(string)))
    ret = ""
    i = 0
    while (i<len(param_list)):
        #print(i)
        ret += str(string[i])+str(param_list[i])
        i+=1
    ret += str(string[i])
    #print(ret)
    return ret # injects data to string

def sticky_str(list_inp,items):
    ret = ""
    count = 0
    for i in list_inp:
        if i == 1:
            ret += str(items[count])
        count += 1
    #print(ret)
    return ret # creates sticky string by given inputs

def Show_res(string,H,W):

    sub_page = tkinter.Tk()
    title = "Result String"
    sub_page.title(title)
    text = tkinter.Text(sub_page, height = H,width = W,exportselection=0)
    text.insert(1.0,string)
    text.pack()
    text.configure(state = tkinter.DISABLED)
    text.configure(inactiveselectbackground = text.cget("selectbackground"))
    sub_page.mainloop()

    return # shows result string

def generate_uuid(entry): # letter: 97-102, number: 0-9
    id = str(uuid.uuid4())
    entry.delete(0, tkinter.END)
    entry.insert(0, id)
    log(str("(SM MOD TOOL)\nRandomized UUID: " + id+ "\nNEXT;\n\n"))
    return # random uuid

def GetAll(uuid_entry,texture1_entry,texture2_entry,texture3_entry,mesh_entry,  pose0_entry,   script_name_inp,scrip_classnem_inp,   exp_destr_lvl_inp,exp_destr_radius_inp,impul_radius_inp,impul_magnt_inp,eff_explod_inp,eff_start_inp,  qualityLevel_inp,color_inp,  dense_in,  material,rotate,  x_in,y_in,z_in   ,diameter_in, depth_in, marg_in, axis_in   ,h_x_in,h_y_in,h_z_in,Pl1_x_in,Pl1_y_in,Pl1_z_in,Pl2_x_in ,Pl2_y_in ,Pl2_z_in ,Pl3_x_in ,Pl3_y_in,Pl3_z_in,  x,x1,y,y1,z,z1):
    global pose0_op
    global script_op
    global explosive_op
    global shape
    # cases
    global main_string
    global pos0
    global box
    global cylinder
    global hull
    global explosive
    global scripted
    global sticky
    # strings

    uuid_entry = uuid_entry.get()
    texture1_entry = texture1_entry.get()
    texture2_entry = texture2_entry.get()
    texture3_entry = texture3_entry.get()
    mesh_entry = mesh_entry.get()

    pose_str = ""
    script_str = ""
    expl_str = ""
    if pose0_op:
        pose0_entry = pose0_entry.get()
        pose_str = inject([pose0_entry],pos0," - ")
    if explosive_op:
        exp_destr_lvl_inp = exp_destr_lvl_inp.get()
        exp_destr_radius_inp = exp_destr_radius_inp.get()
        impul_magnt_inp = impul_magnt_inp.get()
        impul_radius_inp = impul_radius_inp.get()
        eff_explod_inp = eff_explod_inp.get()
        eff_start_inp = eff_start_inp.get()
        expl_str = inject([exp_destr_lvl_inp,exp_destr_radius_inp,impul_radius_inp,impul_magnt_inp,eff_explod_inp,eff_start_inp], explosive, " - ")
    if script_op:
        script_name_inp = script_name_inp.get()
        scrip_classnem_inp = scrip_classnem_inp.get()
        script_str = inject([script_name_inp,scrip_classnem_inp,expl_str],scripted," - ")
    

    qualityLevel_inp = qualityLevel_inp.get()
    color_inp = color_inp.get()
    dense_in = dense_in.get()

    material = material.get(tkinter.ACTIVE)
    rotate = rotate.get(tkinter.ACTIVE)

    D3_str = ""
    #print(shape)
    if (shape == "CYL"):
        diameter_in = diameter_in.get()
        depth_in = depth_in.get()
        marg_in = marg_in.get()
        axis_in = axis_in.get()
        D3_str = inject([diameter_in,depth_in,marg_in,axis_in],cylinder," - ")
    elif (shape == "HUL"):
        h_x_in = h_x_in.get()
        h_y_in = h_y_in.get()
        h_z_in = h_z_in.get()

        Pl1_x_in = Pl1_x_in.get()
        Pl1_y_in = Pl1_y_in.get()
        Pl1_z_in = Pl1_z_in.get()

        Pl2_x_in = Pl2_x_in.get()
        Pl2_y_in = Pl2_y_in.get()
        Pl2_z_in = Pl2_z_in.get()

        Pl3_x_in = Pl3_x_in.get()
        Pl3_y_in = Pl3_y_in.get()
        Pl3_z_in = Pl3_z_in.get()

        D3_str = inject([h_x_in,h_y_in,h_z_in,Pl1_x_in,Pl1_y_in,Pl1_z_in,Pl2_x_in,Pl2_y_in,Pl2_z_in,Pl3_x_in,Pl3_y_in,Pl3_z_in],hull," - ")
    elif (shape == "BOX"):
        x_in = x_in.get()
        y_in = y_in.get()
        z_in = z_in.get()
        #print(box)
        D3_str = inject([x_in,y_in,z_in],box," - ")

        

    x = x.get()
    x1 = x1.get()
    y = y.get()
    y1 = y1.get()
    z = z.get()
    z1 = z1.get()
    
    stick = sticky_str([x,x1,y,y1,z,z1],sticky)

    whole = inject([uuid_entry,texture1_entry,texture2_entry,texture3_entry,mesh_entry,pose_str,script_str,qualityLevel_inp,color_inp,D3_str,dense_in,material,rotate,stick],main_string," - ")

    #print("Whole:\n\n"+whole+"\n\n")
    log(str("(SM MOD TOOL)\nGenerated Part: "+whole+"\nNEXT;\n\n"))
    Show_res(whole,50,90)
    
    return # creates the end string

def description(uuid_str,status):
    global descr
    #print(ret)
    log(str("(SM MOD TOOL)\n---Description Mod Tool StartUp---\nNEXT;\n\n"))
    def show():

        page = tkinter.Tk()
        page.title("Description")
        id_lab = tkinter.Label(page,text = "UUID:", width = 10)
        id_lab.grid(row = 0,column = 0, sticky = tkinter.E)
        id_in = tkinter.Entry(page, width = 50)
        id_in.grid(row = 0, column = 1, sticky = tkinter.W)
       
        rand = tkinter.Button(page,width = 10,text = "Randomize",command = lambda: generate_uuid(id_in))
        rand.grid(row = 0, column = 2, sticky = tkinter.W)

        id_in.delete(0, tkinter.END)
        id_in.insert(0, str(uuid_str))
       
        title_lab = tkinter.Label(page, text = "Title:", width = 10)
        title_lab.grid(row = 1, column = 0, sticky = tkinter.E)
        title_in = tkinter.Entry(page, width = 50)
        title_in.grid(row = 1, column = 1, sticky = tkinter.W)

        desc_lab = tkinter.Label(page, text = "Description:", width = 10)
        desc_lab.grid(row = 2, column = 0, sticky = tkinter.E)
        desc_in = tkinter.Entry(page, width = 50)
        desc_in.grid(row = 2, column = 1, sticky = tkinter.W)

        gen = tkinter.Button(page, text = "Generate", width = 15, command = lambda: generate(id_in,title_in,desc_in))
        gen.grid(row = 3, column = 1, sticky = tkinter.E)

        back = tkinter.Button(page, text = "Back", width = 10, command = lambda: close(status,page))
        back.grid(row = 3, column = 2, sticky = tkinter.W)

        page.mainloop()
        return # window show

    def generate(id_in,title_in,desc_in):
        
        if (uuid_str == ""):
            result = inject([id_in.get(),title_in.get(),desc_in.get()],descr, " - ")
        else:
            result = inject([uuid_str,title_in.get(),desc_in.get()],descr," - ")
        Show_res(result,10,50)
        log(str("(SM MOD TOOL)\nGenerated description: "+result+"\nNEXT;\n\n"))
        return # string generate

    def close(param, window):
        log(str("(SM MOD TOOL)\n***Description Mod Tool CloseDown***\nNEXT;\n\n"))
        if param == 1:
            window.destroy()
        elif param == 2:
            window.destroy()
            modTool_main_func()
        return
    show()
    return # description string generator (status: 1 = floating. 2 = standalone)

def get_hex(view,hex_inp,r_inp,g_inp,b_inp, read_from_hex):
    r = ""
    g = ""
    b = ""
    #print(hex_inp.get(),r_inp.get(), g_inp.get(), b_inp.get(),read_from_hex,"\n>>",str(hex(int(r_inp.get())))[2:],str(hex(int(g_inp.get())))[2:],str(hex(int(b_inp.get())))[2:])
    if (len(r_inp.get())>1 and int(r_inp.get())>255) or (len(g_inp.get())>1 and int(g_inp.get())>255) or (len(b_inp.get())>1 and int(b_inp.get())>255) or (len(r_inp.get())>1 and int(r_inp.get())<0) or (len(g_inp.get())>1 and int(g_inp.get())<0) or (len(b_inp.get())>1 and int(b_inp.get())<0):
        if len(r_inp.get())>1 and int(r_inp.get())>255:
            r_inp.delete(0,tkinter.END)
            r_inp.insert(0, "255")
        elif (len(r_inp.get())>1 and int(r_inp.get())<0):
            r_inp.delete(0,tkinter.END)
            r_inp.insert(0, "0")

        if len(g_inp.get())>1 and int(g_inp.get())>255:
            g_inp.delete(0,tkinter.END)
            g_inp.insert(0, "255")
        elif (len(g_inp.get())>1 and int(g_inp.get())<0):
            g_inp.delete(0,tkinter.END)
            g_inp.insert(0, "0")

        if (len(b_inp.get())>1 and int(b_inp.get())>255):
            b_inp.delete(0,tkinter.END)
            b_inp.insert(0, "255")
        elif (len(b_inp.get())>1 and int(b_inp.get())<0):
            b_inp.delete(0,tkinter.END)
            b_inp.insert(0, "0")
        pop_err("One of the RGB inputs is incorrect!\n(Too big/small or too long/short)",5,30,"ERR_incorrect_RGB_input_color_picker")
        return
    else:
        
        ret_hex = ""
        if not read_from_hex:
            r = str(hex(int(r_inp.get())))[2:]
            if len(r) < 2:
                r = "0" + r
            #print("r"+r)

            
            g = str(hex(int(g_inp.get())))[2:]
            if len(g) < 2:
                g = "0" + g
            #print("g"+g)

            b = str(hex(int(b_inp.get())))[2:]
            if len(b) < 2:
                b = "0" + b
            #print("b"+b)
            ret_hex = "#"+r+g+b
            hex_inp.delete(0, tkinter.END)
            hex_inp.insert(0,ret_hex)
        else:
            #print("hex")
            if (hex_inp.get().startswith("#") and not(len(hex_inp.get()) == 7)) or (not(hex_inp.get().startswith("#")) and not(len(hex_inp.get()) == 6)):
                get_hex(view,hex_inp,r_inp,g_inp,b_inp, False)
                pop_err("One of the RGB inputs is incorrect!\n(Too big/small or too long/short)",5,30,"ERR_incorrect_RGB_input_color_picker")
                return
            if hex_inp.get().startswith("#"):
                ret_hex = hex_inp.get()
            else:
                ret_hex = "#"+hex_inp.get()
                hex_inp.delete(0, tkinter.END)
                hex_inp.insert(0,ret_hex)
            r_inp.delete(0,tkinter.END)
            r_inp.insert(0, int(ret_hex[1:3],16))
            g_inp.delete(0,tkinter.END)
            g_inp.insert(0, int(ret_hex[3:5],16))
            b_inp.delete(0,tkinter.END)
            b_inp.insert(0, int(ret_hex[5:],16))
        #print(ret_hex)
        #show_color(ret_hex)
        view.configure(bg = ret_hex, text = ret_hex)
        log(str("(SM MOD TOOL)\nGenerated color: "+ret_hex+"\nNEXT;\n\n"))
    return # get hex color value from inputs

def incr(page):
    temp = int(page.get())
    if (temp < 255):
        page.delete(0,tkinter.END)
        page.insert(0, str(temp+1))
    return # increase page entry value by 1
def decr(page):
    temp = int(page.get())
    if (temp > 0):
        page.delete(0,tkinter.END)
        page.insert(0, str(temp-1))
    return # decrease page entry value by 1

def reset(hex_view,hex_in, r_inp,g_inp,b_inp):
    hex_in.delete(0,tkinter.END)
    hex_in.insert(0, "#ffffff")
    r_inp.delete(0,tkinter.END)
    r_inp.insert(0,"255")
    g_inp.delete(0,tkinter.END)
    g_inp.insert(0,"255")
    b_inp.delete(0,tkinter.END)
    b_inp.insert(0,"255")

    hex_view.configure(bg = "#ffffff", text = "#ffffff")
    log(str("(SM MOD TOOL)\nColors reseted\nNEXT;\n\n"))
    return # ressets color picker hex, r,g,b values

def color_picker():
    log(str("(SM MOD TOOL)\n---Color Picker StartUp---\nNEXT;\n\n"))
    page = tkinter.Tk()
    page.title("Color Picker")
    text = tkinter.Label(page, text = "Choose R,G and B value,\nor enter a hex code: ")
    r_frame = tkinter.Frame(page)
    g_frame = tkinter.Frame(page)
    b_frame = tkinter.Frame(page)
    r_frame_left = tkinter.Frame(r_frame)
    g_frame_left = tkinter.Frame(g_frame)
    b_frame_left = tkinter.Frame(b_frame)
    r_frame_left.pack(side = tkinter.LEFT)
    g_frame_left.pack(side = tkinter.LEFT)
    b_frame_left.pack(side = tkinter.LEFT)
    r_frame_right = tkinter.Frame(r_frame)
    g_frame_right = tkinter.Frame(g_frame)
    b_frame_right = tkinter.Frame(b_frame)
    r_frame_right.pack(side = tkinter.RIGHT)
    g_frame_right.pack(side = tkinter.RIGHT)
    b_frame_right.pack(side = tkinter.RIGHT)



    R = tkinter.Label(r_frame_left,text = "R: ")
    G = tkinter.Label(g_frame_left,text = "G: ")
    B = tkinter.Label(b_frame_left,text = "B: ")

    r_inp = tkinter.Entry(r_frame_left,width = 10)
    g_inp = tkinter.Entry(g_frame_left,width = 10)
    b_inp = tkinter.Entry(b_frame_left,width = 10)

    

    text.grid(row = 0, column = 0, sticky = tkinter.W)
    r_frame.grid(row = 1, column = 0, sticky = tkinter.W,padx = 5)
    g_frame.grid(row = 2, column = 0, sticky = tkinter.W,padx = 5)
    b_frame.grid(row = 3, column = 0, sticky = tkinter.W,padx = 5)
    
    R.pack(side = tkinter.LEFT)
    G.pack(side = tkinter.LEFT)
    B.pack(side = tkinter.LEFT)
    r_inp.pack(side = tkinter.RIGHT)
    g_inp.pack(side = tkinter.RIGHT)
    b_inp.pack(side = tkinter.RIGHT)

    hex_frame = tkinter.Frame(page)

    hex_text = tkinter.Label(hex_frame, text = "Hex: ")
    hex_in = tkinter.Entry(hex_frame,width = 20)

    gap = tkinter.Label(page).grid(row = 4,rowspan = 2,column = 0, sticky = tkinter.W)

    hex_frame.grid(row = 6, column = 0, sticky = tkinter.W)

    hex_text.grid(row = 0, column = 0, sticky = tkinter.E)
    hex_in.grid(row = 0, column = 1, sticky = tkinter.W)

    hex_view = tkinter.Label(page, bg = "#ffffff", width = 8, text = "#ffffff", height = 5)
    hex_view.grid(row = 1, column = 2, rowspan = 3, sticky = tkinter.E)

    hex_in.delete(0,tkinter.END)
    hex_in.insert(0, "#ffffff")
    r_inp.insert(0,"255")
    g_inp.insert(0,"255")
    b_inp.insert(0,"255")

    reset_button = tkinter.Button(page, text = "Reset", command = lambda: (reset(hex_view,hex_in, r_inp,g_inp,b_inp)))
    reset_button.grid(row = 0, column = 2, sticky = tkinter.NE)

    r_up = tkinter.Button(r_frame_right, text = ">", command = lambda: (incr(r_inp),get_hex(hex_view,hex_in, r_inp,g_inp,b_inp,False)))
    r_up.pack(side = tkinter.RIGHT)
    r_down = tkinter.Button(r_frame_right, text = "<", command = lambda: (decr(r_inp),get_hex(hex_view,hex_in, r_inp,g_inp,b_inp,False)))
    r_down.pack(side = tkinter.LEFT)
    
    g_up = tkinter.Button(g_frame_right, text = ">", command = lambda: (incr(g_inp),get_hex(hex_view,hex_in, r_inp,g_inp,b_inp,False)))
    g_up.pack(side = tkinter.RIGHT)
    g_down = tkinter.Button(g_frame_right, text = "<", command = lambda: (decr(g_inp),get_hex(hex_view,hex_in, r_inp,g_inp,b_inp,False)))
    g_down.pack(side = tkinter.LEFT)

    b_up = tkinter.Button(b_frame_right, text = ">", command = lambda: (incr(b_inp),get_hex(hex_view,hex_in, r_inp,g_inp,b_inp,False)))
    b_up.pack(side = tkinter.RIGHT)
    b_down = tkinter.Button(b_frame_right, text = "<", command = lambda: (decr(b_inp),get_hex(hex_view,hex_in, r_inp,g_inp,b_inp,False)))
    b_down.pack(side = tkinter.LEFT)


    gen_buton = tkinter.Button(page, text = "Generate From Hex", command = lambda: get_hex(hex_view,hex_in, r_inp,g_inp,b_inp,True))
    gen_buton.grid(row = 7, column = 0, sticky = tkinter.E)

    back = tkinter.Button(page, text = "Back", command = lambda: (log(str("(SM MOD TOOL)\n***Color Picker CloseDown***\nNEXT;\n\n")),page.destroy()))
    back.grid(row = 8, column = 0, sticky = tkinter.E)

    

    page.mainloop()
    return # maneges color getting functions and window

def gen_block(uuid_entry, texture1_entry, texture2_entry, texture3_entry, qualityLevel_inp, color_inp, dense_in, material, tile_in):
    global block_string
    uuid_entry = uuid_entry.get()
    texture1_entry = texture1_entry.get()
    texture2_entry = texture2_entry.get()
    texture3_entry = texture3_entry.get()

    qualityLevel_inp = qualityLevel_inp.get()
    color_inp = color_inp.get()
    dense_in = dense_in.get()

    tile_in = tile_in.get()

    ret = inject([uuid_entry,texture1_entry,texture2_entry,texture3_entry,color_inp,dense_in,tile_in,material,qualityLevel_inp],block_string," - ")
    log(str("(SM MOD TOOL)\nGenerated Block: "+ret+"\nNEXT;\n\n"))
    Show_res(ret, 20, 50)

    return # generates block string using inputs

def toFormat(list):
    t = list[0].split(",")
    t.append(list[1])      
    return t # arrange list to format to be used with inject

def eff_gen(path,eff,type_inp,name,attach):
    file = open(path,"r")
    a = file.read()
    file.close()
    if (len(a) == 0):
        pop_err("File Is Empty!",3,15,"MOD_TOOL_EFF_FILE_EMPTY")
        return
    a = a.split("\n")
    if (a.count("") > 0):
        for i in range(a.count("")):
            a = a.remove("")
    for i in range(len(a)):
        a[i] = a[i].split(" ")
    for i in range(len(a)):
        a[i] = toFormat(a[i])


    ret = ""
    file.close()
    for i in range(len(a)):
        ret += inject([type_inp,name,a[i][0],a[i][1],a[i][2],attach,a[i][3]],eff," - ")

    file = open("Effect_output.txt","w+")
    file.write(ret)
    file.close()
    log(str("(SM MOD TOOL)\nGenerated effect file\nName: Effect_output.txt\nNEXT;\n\n"))
    #print(ret)
    return # generates effect file using inputs anf inp file

def eff_help(txt,hght,wid):
    page = tkinter.Tk()
    page.title("Help")
    frame = tkinter.Frame(page)
    frame.pack(side = tkinter.BOTTOM)

    lab = tkinter.Label(page,height = hght,width = wid,text = txt)
    exit = tkinter.Button(frame, text = "Close", width = wid, command = lambda: close_window(page,0))

    lab.pack(side = tkinter.TOP)
    exit.pack(side = tkinter.RIGHT)
    page.mainloop()
    return # displays help for effect file format

def effect_func():
    log(str("(SM MOD TOOL)\n---Effect Mod Tool StartUp---\nNEXT;\n\n"))
    global effect_string
    page = tkinter.Tk()
    page.title("Effect Mod Tool")

    type_sh = tkinter.Label(page,text = "Type:",height = 1)
    type_sh.grid(row = 0, column = 0, sticky = tkinter.E)

    type_inp = tkinter.Entry(page,width = 20)
    type_inp.grid(row = 0, column = 1, sticky = tkinter.W)

    name_sh = tkinter.Label(page,text = "Name:",height = 1)
    name_sh.grid(row = 1, column = 0, sticky = tkinter.E)
    
    name_inp = tkinter.Entry(page,width = 20)
    name_inp.grid(row = 1, column = 1, sticky = tkinter.W)

    path_sh = tkinter.Label(page,text = "XYZ List File Path:",height = 1)
    path_sh.grid(row = 2, column = 0, sticky = tkinter.E)

    path_inp = tkinter.Entry(page, width = 25)
    path_inp.grid(row = 2, column = 1, sticky = tkinter.W)
    path_help = tkinter.Button(page,text = "?",fg = "#ffffff",font = ("daivd", 9, "bold"),bg = "#3179ed",width = 2, height = 1,command = lambda: (eff_help('Get all the XYZ 3D coordinates and the delays,\nand put them in a text file in the format:\n"X,Y,Z Delay\nX,Y,Z Delay\n..."',7,34)))
    path_help.grid(row = 2, column = 3, sticky =tkinter.E)

    atach_sh = tkinter.Label(page, text = "Attach:", height = 1)
    atach_sh.grid(row = 3, column = 0, sticky = tkinter.E)

    atach_inp = tkinter.Listbox(page, width = 10, height = 1)
    atach_inp.grid(row = 3, column = 1, sticky = tkinter.W)
    atach_inp.insert(tkinter.END,"true")
    atach_inp.insert(tkinter.END,"false")

    atach_help = tkinter.Label(page, text = "Default is true", height = 1)
    atach_help.grid(row = 3, column = 3, sticky = tkinter.E)

    gen_butt = tkinter.Button(page, text = "Generate File",width = 10, height = 1, command = lambda: (eff_gen(path_inp.get(),effect_string,type_inp.get(),name_inp.get(),atach_inp.get(tkinter.ACTIVE))))
    gen_butt.grid(row =  4, column = 1, sticky = tkinter.E)

    back = tkinter.Button(page, text = "Back",width = 10, height = 1, command = lambda: (log(str("(SM MOD TOOL)\n***Effect Mod Tool CloseDown***\nNEXT;\n\n")),close_window(page,2)))
    back.grid(row = 4, column = 3, sticky = tkinter.W)
    page.mainloop()
    return # ******************** sm effect tool
# sm modding functions *********************************************


def calculatorFunc():

    
    calculator = tkinter.Tk()

    calculator.title("Calculator")
    create_widgets(calculator)

    # Make window fixed (cannot be resized)
    calculator.resizable(width = False, height = False)

    calculator.mainloop()
    
    return # calculator

# ping tool *************

def ping_func():

    

    dnsDstIP="8.8.8.8"
    dnsDstPort=53
    StartsWith = "www."

    def nslookup(addres):
        if (not addres.startswith(StartsWith)):
            addres = StartsWith + addres
        dnsAnswer = sr1(IP(dst = dnsDstIP) / UDP(dport = dnsDstPort) / DNS(rd = 1, qd = DNSQR(qname = addres)))#, verbose = 0)
        print(dnsAnswer)
        return dnsAnswer[DNS][DNSRR].rdata
    
    def average(list):
        count = 0
        average = 0
        for i in list:
            average += i
            count+=1
        return str(int(average/count))

    def ping(address, code = 0, val = 0,secCode = 0,secVal = 0):
        if (not address.startswith(StartsWith)):
            addressFixed = StartsWith + address
        else:
            addressFixed = address
        ttl = 64
        pingCount = 1

        #ipAddress = nslookup(addressFixed) # grtting the host IP
        ipAddress = "192.168.43.1"
        pingMsg = IP(dst=ipAddress,ttl = ttl) / ICMP()
        timeSave = []
        msgLost = 0
        print("\nPinging",address,"["+ipAddress+"] with",len(pingMsg),"bytes of data:")

        for i in range(pingCount):
            strtTime = time.time()
            pingAns = sr1(pingMsg, verbose = 0,timeout = 3) # sending the message and recording time
            endTime = time.time()
            time.sleep(0.5) # for no dns flood

            if (pingAns == None): # if nothing returned
                print("Request timeout")
                msgLost += 1
            elif (pingAns[ICMP].type == "time-exceeded"): # if ttl expired
                print("Reply from ***router***: TTL expired in transit.")
                msgLost += 1
            else:
                print("Reply from "+ipAddress+": bytes="+str(len(pingMsg))+" time="+str(int((endTime-strtTime)*1000))+"ms TTL="+str(pingMsg[IP].ttl)) # printing the ping

            timeSave.append(int((endTime-strtTime)*1000))
        timeSave.sort()
        print("\nPing statistics for "+ipAddress+":\n    Packets: Sent = "+str(pingCount)+", Received = "+str(pingCount-msgLost)+", Lost = "+str(msgLost)+" ("+str((pingCount/100)*msgLost)+"% loss),\nApproximate round trip times in milli-seconds:\n    Minimum = "+str(timeSave[0]-100)+"ms, Maximum = "+str(timeSave[-1]-100)+"ms, Average = "+average(timeSave-100)+"ms\n") # all the statistics
        return

    def window_manage():

        page = tkinter.Tk()
        page.title("Ping Tool")

        pict = tkinter.PhotoImage(file=".\\image_files\\0_4ping.gif") # --------------------------------------------------------------------------------error
        
        address_txt = tkinter.Label(page,text = "Address:",height = 1, width = 10)
        address_txt.grid(row = 0, column = 0, sticky = tkinter.E)

        address_inp = tkinter.Entry(page, width = 15)
        address_inp.grid(row = 0, column = 1, sticky = tkinter.W)

        temp= tkinter.Label(page,image = pict, width = 10, height = 10)
        temp.grid(row = 1, column = 0, sticky = tkinter.W)
        temp.image = pict
        
        page.mainloop()
        return
    window_manage()
    return

# ping tool *************

#Main() # main function


ping_func()